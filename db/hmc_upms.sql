/*
Navicat MySQL Data Transfer

Source Server         : hmc-rds测试
Source Server Version : 50634
Source Host           : rm-uf6d38vra5qt1hd8ko.mysql.rds.aliyuncs.com:3306
Source Database       : hmc_upms

Target Server Type    : MYSQL
Target Server Version : 50634
File Encoding         : 65001

Date: 2017-11-13 17:32:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for upms_resource
-- ----------------------------
DROP TABLE IF EXISTS `upms_resource`;
CREATE TABLE `upms_resource` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `parent_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '所属上级',
  `system_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'upms_system表主键',
  `name` varchar(32) NOT NULL DEFAULT '' COMMENT '名称',
  `type` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '类型(1:目录,2:菜单,3:按钮)',
  `permission` varchar(128) NOT NULL DEFAULT '' COMMENT '权限值',
  `uri` varchar(128) NOT NULL DEFAULT '' COMMENT '路径',
  `icon` varchar(64) NOT NULL DEFAULT '' COMMENT '图标',
  `status` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '状态(0:禁止,1:正常)',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `gmt_created` datetime NOT NULL COMMENT '创建时间',
  `created_by` varchar(64) NOT NULL DEFAULT '' COMMENT '创建人',
  `gmt_modified` datetime NOT NULL COMMENT '修改人',
  `modified_by` varchar(64) NOT NULL DEFAULT '' COMMENT '修改人',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_permission` (`permission`) USING BTREE,
  KEY `idx_parent_id` (`parent_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8 COMMENT='权限';

-- ----------------------------
-- Records of upms_resource
-- ----------------------------
INSERT INTO `upms_resource` VALUES ('1', '0', '1', '用户', '2', 'sys:user:list', '/sys/job/listVM', '', '1', '1', '2017-07-20 14:37:17', '', '2017-07-20 14:37:17', '');
INSERT INTO `upms_resource` VALUES ('2', '1', '1', '添加用户', '3', 'sys:user:add', '/sys/user/addVM', '', '1', '1', '2017-07-20 14:37:17', '', '2017-07-20 14:37:17', '');
INSERT INTO `upms_resource` VALUES ('3', '1', '1', '编辑用户', '3', 'sys:user:edit', '/sys/user/editVM', '', '1', '1', '2017-07-20 14:37:17', '', '2017-07-20 14:37:17', '');
INSERT INTO `upms_resource` VALUES ('4', '1', '1', '删除用户', '3', 'sys:user:remove', '', '', '1', '1', '2017-07-20 14:37:17', '', '2017-07-20 14:37:17', '');
INSERT INTO `upms_resource` VALUES ('5', '0', '2', 'app1:index', '2', 'app1:index', '', '', '1', '1', '2017-07-20 14:37:17', '', '2017-07-20 14:37:17', '');
INSERT INTO `upms_resource` VALUES ('6', '5', '2', 'app1:index:app1', '3', 'app1:index:app1', '', '', '1', '1', '2017-07-20 14:37:17', '', '2017-07-20 14:37:17', '');
INSERT INTO `upms_resource` VALUES ('7', '0', '3', 'app2:index', '2', 'app2:index', '', '', '1', '1', '2017-07-20 14:37:17', '', '2017-07-20 14:37:17', '');
INSERT INTO `upms_resource` VALUES ('8', '7', '3', 'app2:index:app2', '3', 'app2:index:app2', '', '', '1', '1', '2017-07-20 14:37:17', '', '2017-07-20 14:37:17', '');

-- ----------------------------
-- Table structure for upms_role
-- ----------------------------
DROP TABLE IF EXISTS `upms_role`;
CREATE TABLE `upms_role` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `code` varchar(32) NOT NULL DEFAULT '' COMMENT '角色代码',
  `name` varchar(32) NOT NULL DEFAULT '' COMMENT '角色名称',
  `status` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '是否有效',
  `description` varchar(256) NOT NULL DEFAULT '' COMMENT '角色描述（备注）',
  `created_by` varchar(32) NOT NULL DEFAULT '' COMMENT '创建人',
  `gmt_created` datetime NOT NULL DEFAULT '0000-00-00 08:00:00' COMMENT '创建时间',
  `modified_by` varchar(32) NOT NULL DEFAULT '' COMMENT '修改人',
  `gmt_modified` datetime NOT NULL DEFAULT '0000-00-00 08:00:00' COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_role_name` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8 COMMENT='角色';

-- ----------------------------
-- Records of upms_role
-- ----------------------------
INSERT INTO `upms_role` VALUES ('1', 'upms', 'upms', '1', '', '1', '2017-11-13 14:48:14', '', '2017-11-13 14:48:17');
INSERT INTO `upms_role` VALUES ('2', 'app1', 'app1', '1', '', '1', '2017-11-13 14:48:14', '', '2017-11-13 14:48:17');
INSERT INTO `upms_role` VALUES ('3', 'app2', 'app2', '1', '', '1', '2017-11-13 14:48:14', '', '2017-11-13 14:48:17');

-- ----------------------------
-- Table structure for upms_role_resource
-- ----------------------------
DROP TABLE IF EXISTS `upms_role_resource`;
CREATE TABLE `upms_role_resource` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '角色编号',
  `resource_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '权限编号',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_role_id_resource_id` (`role_id`,`resource_id`) USING BTREE,
  KEY `idx_resource_id` (`resource_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=386 DEFAULT CHARSET=utf8 COMMENT='角色权限关联表';

-- ----------------------------
-- Records of upms_role_resource
-- ----------------------------
INSERT INTO `upms_role_resource` VALUES ('1', '1', '1');
INSERT INTO `upms_role_resource` VALUES ('2', '1', '2');
INSERT INTO `upms_role_resource` VALUES ('3', '1', '3');
INSERT INTO `upms_role_resource` VALUES ('4', '1', '4');
INSERT INTO `upms_role_resource` VALUES ('5', '2', '5');
INSERT INTO `upms_role_resource` VALUES ('6', '2', '6');
INSERT INTO `upms_role_resource` VALUES ('7', '3', '7');
INSERT INTO `upms_role_resource` VALUES ('8', '3', '8');

-- ----------------------------
-- Table structure for upms_system
-- ----------------------------
DROP TABLE IF EXISTS `upms_system`;
CREATE TABLE `upms_system` (
  `id` bigint(20) NOT NULL,
  `name` varchar(64) NOT NULL DEFAULT '' COMMENT '系统名称',
  `status` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '状态(0：未启用，1:正常)',
  `title` varchar(64) NOT NULL DEFAULT '' COMMENT '系统标题',
  `description` varchar(256) NOT NULL DEFAULT '' COMMENT '描述',
  `gmt_created` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of upms_system
-- ----------------------------
INSERT INTO `upms_system` VALUES ('1', 'hmc-upms', '1', 'hmc-upms', 'hmc-upms', '2017-11-09 15:29:21', '2017-11-09 15:29:23');
INSERT INTO `upms_system` VALUES ('2', 'hmc-app1', '1', 'hmc-app1', 'hmc-app1', '2017-11-09 15:29:21', '2017-11-09 15:29:23');
INSERT INTO `upms_system` VALUES ('3', 'hmc-app2', '1', 'hmc-app2', 'hmc-app2', '2017-11-09 15:29:21', '2017-11-09 15:29:23');

-- ----------------------------
-- Table structure for upms_user
-- ----------------------------
DROP TABLE IF EXISTS `upms_user`;
CREATE TABLE `upms_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `username` varchar(20) NOT NULL DEFAULT '' COMMENT '登录帐号',
  `employee_number` varchar(50) NOT NULL DEFAULT '' COMMENT '员工编号',
  `password` varchar(32) NOT NULL DEFAULT '' COMMENT '密码MD5(密码+盐)',
  `salt` varchar(32) NOT NULL DEFAULT '' COMMENT '盐',
  `real_name` varchar(20) NOT NULL DEFAULT '' COMMENT '真实姓名',
  `job_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '职位',
  `organization_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '组织id',
  `avatar` varchar(128) NOT NULL DEFAULT '' COMMENT '头像',
  `phone` varchar(16) NOT NULL DEFAULT '' COMMENT '电话',
  `email` varchar(64) NOT NULL DEFAULT '' COMMENT '邮箱',
  `locked` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '登录权限状态(0:正常,1:锁定)',
  `employee_attr` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '员工属性 1：用户 2：系统管理员 3：外来人员',
  `is_first_login` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '首次登陆 0：否 1：是',
  `gmt_created` datetime NOT NULL COMMENT '创建时间',
  `created_by` varchar(32) NOT NULL DEFAULT '' COMMENT '创建人',
  `gmt_modified` datetime NOT NULL COMMENT '修改时间',
  `modified_by` varchar(32) NOT NULL DEFAULT '' COMMENT '修改人',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_username` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8 COMMENT='用户';

-- ----------------------------
-- Records of upms_user
-- ----------------------------
INSERT INTO `upms_user` VALUES ('1', 'test', 'test', 'ea5de0835d6b16214800f7708ad6a602', '3291c4c99b9e54015e21348227e04769', '', '0', '0', '', '', '', '0', '0', '1', '2017-11-08 16:11:13', '', '2017-11-08 16:11:16', '');
INSERT INTO `upms_user` VALUES ('2', 'app1', 'app1', 'ea5de0835d6b16214800f7708ad6a602', '3291c4c99b9e54015e21348227e04769', '', '0', '0', '', '', '', '0', '0', '1', '2017-11-08 16:11:13', '', '2017-11-08 16:11:16', '');
INSERT INTO `upms_user` VALUES ('3', 'app2', 'app2', 'ea5de0835d6b16214800f7708ad6a602', '3291c4c99b9e54015e21348227e04769', '', '0', '0', '', '', '', '0', '0', '1', '2017-11-08 16:11:13', '', '2017-11-08 16:11:16', '');

-- ----------------------------
-- Table structure for upms_user_role
-- ----------------------------
DROP TABLE IF EXISTS `upms_user_role`;
CREATE TABLE `upms_user_role` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '角色id',
  `gmt_created` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_user_id_role_id` (`user_id`,`role_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=utf8 COMMENT='用户角色关联表';

-- ----------------------------
-- Records of upms_user_role
-- ----------------------------
INSERT INTO `upms_user_role` VALUES ('1', '1', '1', '2017-11-13 17:22:42', '2017-11-13 17:22:45');
INSERT INTO `upms_user_role` VALUES ('2', '1', '2', '2017-11-13 17:22:53', '2017-11-13 17:22:56');
INSERT INTO `upms_user_role` VALUES ('3', '1', '3', '2017-11-13 17:23:04', '2017-11-13 17:23:06');
INSERT INTO `upms_user_role` VALUES ('4', '2', '2', '2017-11-13 17:23:16', '2017-11-13 17:23:18');
INSERT INTO `upms_user_role` VALUES ('5', '3', '3', '2017-11-13 17:23:29', '2017-11-13 17:23:31');
