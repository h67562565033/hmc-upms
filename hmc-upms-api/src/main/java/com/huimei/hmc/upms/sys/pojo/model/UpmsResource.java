package com.huimei.hmc.upms.sys.pojo.model;

import java.util.Date;
import javax.persistence.*;

@Table(name = "upms_resource")
public class UpmsResource {
    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 所属上级
     */
    @Column(name = "parent_id")
    private Long parentId;

    /**
     * upms_system表主键
     */
    @Column(name = "system_id")
    private Long systemId;

    /**
     * 名称
     */
    private String name;

    /**
     * 类型(1:目录,2:菜单,3:按钮)
     */
    private Byte type;

    /**
     * 权限值
     */
    private String permission;

    /**
     * 路径
     */
    private String uri;

    /**
     * 图标
     */
    private String icon;

    /**
     * 状态(0:禁止,1:正常)
     */
    private Byte status;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 创建时间
     */
    @Column(name = "gmt_created")
    private Date gmtCreated;

    /**
     * 创建人
     */
    @Column(name = "created_by")
    private String createdBy;

    /**
     * 修改人
     */
    @Column(name = "gmt_modified")
    private Date gmtModified;

    /**
     * 修改人
     */
    @Column(name = "modified_by")
    private String modifiedBy;

    /**
     * 获取主键
     *
     * @return id - 主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置主键
     *
     * @param id 主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取所属上级
     *
     * @return parent_id - 所属上级
     */
    public Long getParentId() {
        return parentId;
    }

    /**
     * 设置所属上级
     *
     * @param parentId 所属上级
     */
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    /**
     * 获取upms_system表主键
     *
     * @return system_id - upms_system表主键
     */
    public Long getSystemId() {
        return systemId;
    }

    /**
     * 设置upms_system表主键
     *
     * @param systemId upms_system表主键
     */
    public void setSystemId(Long systemId) {
        this.systemId = systemId;
    }

    /**
     * 获取名称
     *
     * @return name - 名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置名称
     *
     * @param name 名称
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 获取类型(1:目录,2:菜单,3:按钮)
     *
     * @return type - 类型(1:目录,2:菜单,3:按钮)
     */
    public Byte getType() {
        return type;
    }

    /**
     * 设置类型(1:目录,2:菜单,3:按钮)
     *
     * @param type 类型(1:目录,2:菜单,3:按钮)
     */
    public void setType(Byte type) {
        this.type = type;
    }

    /**
     * 获取权限值
     *
     * @return permission - 权限值
     */
    public String getPermission() {
        return permission;
    }

    /**
     * 设置权限值
     *
     * @param permission 权限值
     */
    public void setPermission(String permission) {
        this.permission = permission == null ? null : permission.trim();
    }

    /**
     * 获取路径
     *
     * @return uri - 路径
     */
    public String getUri() {
        return uri;
    }

    /**
     * 设置路径
     *
     * @param uri 路径
     */
    public void setUri(String uri) {
        this.uri = uri == null ? null : uri.trim();
    }

    /**
     * 获取图标
     *
     * @return icon - 图标
     */
    public String getIcon() {
        return icon;
    }

    /**
     * 设置图标
     *
     * @param icon 图标
     */
    public void setIcon(String icon) {
        this.icon = icon == null ? null : icon.trim();
    }

    /**
     * 获取状态(0:禁止,1:正常)
     *
     * @return status - 状态(0:禁止,1:正常)
     */
    public Byte getStatus() {
        return status;
    }

    /**
     * 设置状态(0:禁止,1:正常)
     *
     * @param status 状态(0:禁止,1:正常)
     */
    public void setStatus(Byte status) {
        this.status = status;
    }

    /**
     * 获取排序
     *
     * @return sort - 排序
     */
    public Integer getSort() {
        return sort;
    }

    /**
     * 设置排序
     *
     * @param sort 排序
     */
    public void setSort(Integer sort) {
        this.sort = sort;
    }

    /**
     * 获取创建时间
     *
     * @return gmt_created - 创建时间
     */
    public Date getGmtCreated() {
        return gmtCreated;
    }

    /**
     * 设置创建时间
     *
     * @param gmtCreated 创建时间
     */
    public void setGmtCreated(Date gmtCreated) {
        this.gmtCreated = gmtCreated;
    }

    /**
     * 获取创建人
     *
     * @return created_by - 创建人
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * 设置创建人
     *
     * @param createdBy 创建人
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy == null ? null : createdBy.trim();
    }

    /**
     * 获取修改人
     *
     * @return gmt_modified - 修改人
     */
    public Date getGmtModified() {
        return gmtModified;
    }

    /**
     * 设置修改人
     *
     * @param gmtModified 修改人
     */
    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    /**
     * 获取修改人
     *
     * @return modified_by - 修改人
     */
    public String getModifiedBy() {
        return modifiedBy;
    }

    /**
     * 设置修改人
     *
     * @param modifiedBy 修改人
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy == null ? null : modifiedBy.trim();
    }
}