package com.huimei.hmc.upms.sys.pojo.model;

import java.util.Date;
import javax.persistence.*;

@Table(name = "upms_user")
public class UpmsUser {
    /**
     * 编号
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 登录帐号
     */
    private String username;

    /**
     * 员工编号
     */
    @Column(name = "employee_number")
    private String employeeNumber;

    /**
     * 密码MD5(密码+盐)
     */
    private String password;

    /**
     * 盐
     */
    private String salt;

    /**
     * 真实姓名
     */
    @Column(name = "real_name")
    private String realName;

    /**
     * 职位
     */
    @Column(name = "job_id")
    private Long jobId;

    /**
     * 组织id
     */
    @Column(name = "organization_id")
    private Long organizationId;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 电话
     */
    private String phone;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 登录权限状态(0:正常,1:锁定)
     */
    private Byte locked;

    /**
     * 员工属性 1：用户 2：系统管理员 3：外来人员
     */
    @Column(name = "employee_attr")
    private Byte employeeAttr;

    /**
     * 首次登陆 0：否 1：是
     */
    @Column(name = "is_first_login")
    private Boolean isFirstLogin;

    /**
     * 创建时间
     */
    @Column(name = "gmt_created")
    private Date gmtCreated;

    /**
     * 创建人
     */
    @Column(name = "created_by")
    private String createdBy;

    /**
     * 修改时间
     */
    @Column(name = "gmt_modified")
    private Date gmtModified;

    /**
     * 修改人
     */
    @Column(name = "modified_by")
    private String modifiedBy;

    /**
     * 获取编号
     *
     * @return id - 编号
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置编号
     *
     * @param id 编号
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取登录帐号
     *
     * @return username - 登录帐号
     */
    public String getUsername() {
        return username;
    }

    /**
     * 设置登录帐号
     *
     * @param username 登录帐号
     */
    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    /**
     * 获取员工编号
     *
     * @return employee_number - 员工编号
     */
    public String getEmployeeNumber() {
        return employeeNumber;
    }

    /**
     * 设置员工编号
     *
     * @param employeeNumber 员工编号
     */
    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber == null ? null : employeeNumber.trim();
    }

    /**
     * 获取密码MD5(密码+盐)
     *
     * @return password - 密码MD5(密码+盐)
     */
    public String getPassword() {
        return password;
    }

    /**
     * 设置密码MD5(密码+盐)
     *
     * @param password 密码MD5(密码+盐)
     */
    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    /**
     * 获取盐
     *
     * @return salt - 盐
     */
    public String getSalt() {
        return salt;
    }

    /**
     * 设置盐
     *
     * @param salt 盐
     */
    public void setSalt(String salt) {
        this.salt = salt == null ? null : salt.trim();
    }

    /**
     * 获取真实姓名
     *
     * @return real_name - 真实姓名
     */
    public String getRealName() {
        return realName;
    }

    /**
     * 设置真实姓名
     *
     * @param realName 真实姓名
     */
    public void setRealName(String realName) {
        this.realName = realName == null ? null : realName.trim();
    }

    /**
     * 获取职位
     *
     * @return job_id - 职位
     */
    public Long getJobId() {
        return jobId;
    }

    /**
     * 设置职位
     *
     * @param jobId 职位
     */
    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    /**
     * 获取组织id
     *
     * @return organization_id - 组织id
     */
    public Long getOrganizationId() {
        return organizationId;
    }

    /**
     * 设置组织id
     *
     * @param organizationId 组织id
     */
    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    /**
     * 获取头像
     *
     * @return avatar - 头像
     */
    public String getAvatar() {
        return avatar;
    }

    /**
     * 设置头像
     *
     * @param avatar 头像
     */
    public void setAvatar(String avatar) {
        this.avatar = avatar == null ? null : avatar.trim();
    }

    /**
     * 获取电话
     *
     * @return phone - 电话
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 设置电话
     *
     * @param phone 电话
     */
    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    /**
     * 获取邮箱
     *
     * @return email - 邮箱
     */
    public String getEmail() {
        return email;
    }

    /**
     * 设置邮箱
     *
     * @param email 邮箱
     */
    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    /**
     * 获取登录权限状态(0:正常,1:锁定)
     *
     * @return locked - 登录权限状态(0:正常,1:锁定)
     */
    public Byte getLocked() {
        return locked;
    }

    /**
     * 设置登录权限状态(0:正常,1:锁定)
     *
     * @param locked 登录权限状态(0:正常,1:锁定)
     */
    public void setLocked(Byte locked) {
        this.locked = locked;
    }

    /**
     * 获取员工属性 1：用户 2：系统管理员 3：外来人员
     *
     * @return employee_attr - 员工属性 1：用户 2：系统管理员 3：外来人员
     */
    public Byte getEmployeeAttr() {
        return employeeAttr;
    }

    /**
     * 设置员工属性 1：用户 2：系统管理员 3：外来人员
     *
     * @param employeeAttr 员工属性 1：用户 2：系统管理员 3：外来人员
     */
    public void setEmployeeAttr(Byte employeeAttr) {
        this.employeeAttr = employeeAttr;
    }

    /**
     * 获取首次登陆 0：否 1：是
     *
     * @return is_first_login - 首次登陆 0：否 1：是
     */
    public Boolean getIsFirstLogin() {
        return isFirstLogin;
    }

    /**
     * 设置首次登陆 0：否 1：是
     *
     * @param isFirstLogin 首次登陆 0：否 1：是
     */
    public void setIsFirstLogin(Boolean isFirstLogin) {
        this.isFirstLogin = isFirstLogin;
    }

    /**
     * 获取创建时间
     *
     * @return gmt_created - 创建时间
     */
    public Date getGmtCreated() {
        return gmtCreated;
    }

    /**
     * 设置创建时间
     *
     * @param gmtCreated 创建时间
     */
    public void setGmtCreated(Date gmtCreated) {
        this.gmtCreated = gmtCreated;
    }

    /**
     * 获取创建人
     *
     * @return created_by - 创建人
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * 设置创建人
     *
     * @param createdBy 创建人
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy == null ? null : createdBy.trim();
    }

    /**
     * 获取修改时间
     *
     * @return gmt_modified - 修改时间
     */
    public Date getGmtModified() {
        return gmtModified;
    }

    /**
     * 设置修改时间
     *
     * @param gmtModified 修改时间
     */
    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    /**
     * 获取修改人
     *
     * @return modified_by - 修改人
     */
    public String getModifiedBy() {
        return modifiedBy;
    }

    /**
     * 设置修改人
     *
     * @param modifiedBy 修改人
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy == null ? null : modifiedBy.trim();
    }
}