package com.huimei.hmc.upms.sys.pojo.model;

import javax.persistence.*;

@Table(name = "upms_role_resource")
public class UpmsRoleResource {
    /**
     * 编号
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 角色编号
     */
    @Column(name = "role_id")
    private Long roleId;

    /**
     * 权限编号
     */
    @Column(name = "resource_id")
    private Long resourceId;

    /**
     * 获取编号
     *
     * @return id - 编号
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置编号
     *
     * @param id 编号
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取角色编号
     *
     * @return role_id - 角色编号
     */
    public Long getRoleId() {
        return roleId;
    }

    /**
     * 设置角色编号
     *
     * @param roleId 角色编号
     */
    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    /**
     * 获取权限编号
     *
     * @return resource_id - 权限编号
     */
    public Long getResourceId() {
        return resourceId;
    }

    /**
     * 设置权限编号
     *
     * @param resourceId 权限编号
     */
    public void setResourceId(Long resourceId) {
        this.resourceId = resourceId;
    }
}