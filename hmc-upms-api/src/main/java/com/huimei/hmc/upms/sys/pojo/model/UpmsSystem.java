package com.huimei.hmc.upms.sys.pojo.model;

import java.util.Date;
import javax.persistence.*;

@Table(name = "upms_system")
public class UpmsSystem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 系统名称
     */
    private String name;

    /**
     * 状态(0：未启用，1:正常)
     */
    private Byte status;

    /**
     * 系统标题
     */
    private String title;

    /**
     * 描述
     */
    private String description;

    /**
     * 创建时间
     */
    @Column(name = "gmt_created")
    private Date gmtCreated;

    /**
     * 修改时间
     */
    @Column(name = "gmt_modified")
    private Date gmtModified;

    /**
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取系统名称
     *
     * @return name - 系统名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置系统名称
     *
     * @param name 系统名称
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 获取状态(0：未启用，1:正常)
     *
     * @return status - 状态(0：未启用，1:正常)
     */
    public Byte getStatus() {
        return status;
    }

    /**
     * 设置状态(0：未启用，1:正常)
     *
     * @param status 状态(0：未启用，1:正常)
     */
    public void setStatus(Byte status) {
        this.status = status;
    }

    /**
     * 获取系统标题
     *
     * @return title - 系统标题
     */
    public String getTitle() {
        return title;
    }

    /**
     * 设置系统标题
     *
     * @param title 系统标题
     */
    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    /**
     * 获取描述
     *
     * @return description - 描述
     */
    public String getDescription() {
        return description;
    }

    /**
     * 设置描述
     *
     * @param description 描述
     */
    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    /**
     * 获取创建时间
     *
     * @return gmt_created - 创建时间
     */
    public Date getGmtCreated() {
        return gmtCreated;
    }

    /**
     * 设置创建时间
     *
     * @param gmtCreated 创建时间
     */
    public void setGmtCreated(Date gmtCreated) {
        this.gmtCreated = gmtCreated;
    }

    /**
     * 获取修改时间
     *
     * @return gmt_modified - 修改时间
     */
    public Date getGmtModified() {
        return gmtModified;
    }

    /**
     * 设置修改时间
     *
     * @param gmtModified 修改时间
     */
    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }
}