package com.huimei.hmc.upms.sys.api;

import com.github.pagehelper.PageInfo;
import com.huimei.hmc.upms.sys.pojo.model.UpmsResource;

/**
 * Created by huangyuheng on 2017/7/18.
 */
public interface IUpmsResourceService {

    int saveUpmsResource(UpmsResource upmsResource);

    PageInfo listUpmsResource(String username, Integer pageNum, Integer pageSize);

    int updateUpmsResource(UpmsResource upmsResource);

    UpmsResource getUpmsResourceById(Long id);

    int removeUpmsResourceById(Long id);

}
