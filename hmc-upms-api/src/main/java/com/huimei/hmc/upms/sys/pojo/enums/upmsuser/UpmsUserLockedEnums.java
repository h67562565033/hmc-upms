package com.huimei.hmc.upms.sys.pojo.enums.upmsuser;

/**
 * Created by huangyuheng on 2017/7/24.
 */
public enum UpmsUserLockedEnums {

    NORMAL((byte) 0, "正常"),

    LOCKED((byte) 1, "锁定");

    private Byte code;
    private String description;

    UpmsUserLockedEnums(Byte code, String description) {
        this.code = code;
        this.description = description;
    }

    public Byte getCode() {
        return code;
    }

    public void setCode(Byte code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
