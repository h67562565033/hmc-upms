package com.huimei.hmc.upms.sys.api;

import com.github.pagehelper.PageInfo;
import com.huimei.hmc.upms.sys.pojo.model.UpmsUser;

import java.util.Set;

/**
 * Created by Huangyuheng on 2017/7/5.
 */
public interface IUpmsUserService {

    int saveUpmsUser(UpmsUser upmsUser);

    int updateUpmsUser(UpmsUser upmsUser);

    UpmsUser getUpmsUserById(Long id);

    int removeUpmsUserById(Long id);

    UpmsUser getUpmsUserByUsername(String username);

    void changeUserPassword(Long id, String oldPassword, String password, String rePassword);

    Set<String> getUpmsUserRoleCode(String username);

    Set<String> getUpmsUserPermission(String username);

}
