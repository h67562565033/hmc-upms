package com.huimei.hmc.upms.sys.pojo.enums.upmsuser;

/**
 * Created by huangyuheng on 2017/7/31.
 */
public enum UpmsUserEmployeeAttrEnums {

    USER((byte) 1, "用户"),

    SYSTEM((byte) 2, "系统管理员"),

    FOREIGN((byte) 3, "外来人员");


    private Byte code;
    private String description;

    UpmsUserEmployeeAttrEnums(Byte code, String description) {
        this.code = code;
        this.description = description;
    }

    public Byte getCode() {
        return code;
    }

    public void setCode(Byte code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
