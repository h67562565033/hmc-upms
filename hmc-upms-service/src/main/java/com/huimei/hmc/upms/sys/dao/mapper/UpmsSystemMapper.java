package com.huimei.hmc.upms.sys.dao.mapper;

import com.huimei.hmc.common.base.BaseHmcMapper;
import com.huimei.hmc.upms.sys.pojo.model.UpmsSystem;

public interface UpmsSystemMapper extends BaseHmcMapper<UpmsSystem> {
}