package com.huimei.hmc.upms.sys.dao.mapper;

import com.huimei.hmc.common.base.BaseHmcMapper;
import com.huimei.hmc.upms.sys.pojo.model.UpmsUser;
import org.apache.ibatis.annotations.Param;

public interface UpmsUserMapper extends BaseHmcMapper<UpmsUser> {

    UpmsUser selectByUsername(@Param("username") String username);

}