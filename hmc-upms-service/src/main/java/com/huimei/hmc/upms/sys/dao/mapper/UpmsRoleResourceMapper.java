package com.huimei.hmc.upms.sys.dao.mapper;

import com.huimei.hmc.common.base.BaseHmcMapper;
import com.huimei.hmc.upms.sys.pojo.model.UpmsRoleResource;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UpmsRoleResourceMapper extends BaseHmcMapper<UpmsRoleResource> {

    List<UpmsRoleResource> selectByRoleId(@Param("roleId") Long roleId);

}