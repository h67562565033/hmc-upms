package com.huimei.hmc.upms.sys.dao.mapper;

import com.huimei.hmc.common.base.BaseHmcMapper;
import com.huimei.hmc.upms.sys.pojo.model.UpmsUserRole;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UpmsUserRoleMapper extends BaseHmcMapper<UpmsUserRole> {

    List<UpmsUserRole> selectByUserId(@Param("userId") Long userId);

}