package com.huimei.hmc.upms.common.enums;

/**
 * Created by Huangyuheng on 2017/9/6.
 */
public enum UpmsResultEnum {

    FAILED("0", "failed"),
    SUCCESS("1", "success");

    private String code;

    private String message;

    UpmsResultEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

}
