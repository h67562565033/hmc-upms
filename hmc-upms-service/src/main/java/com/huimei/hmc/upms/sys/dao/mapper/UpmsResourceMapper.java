package com.huimei.hmc.upms.sys.dao.mapper;

import com.huimei.hmc.common.base.BaseHmcMapper;
import com.huimei.hmc.upms.sys.pojo.model.UpmsResource;

public interface UpmsResourceMapper extends BaseHmcMapper<UpmsResource> {
}