package com.huimei.hmc.upms.sys.web.controller;

import com.huimei.hmc.common.base.BaseResult;
import com.huimei.hmc.upms.client.common.base.BaseUpmsController;
import com.huimei.hmc.upms.sys.api.IUpmsUserService;
import com.huimei.hmc.upms.sys.pojo.model.UpmsUser;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Huangyuheng on 2017/7/5.
 */
@RestController
@RequestMapping("/sys/user")
public class UpmsUserController extends BaseUpmsController {

    @Autowired
    private IUpmsUserService upmsUserService;

    @RequiresPermissions("sys:user:add")
    @RequestMapping(value = "/add")
    public BaseResult add(UpmsUser upmsUser) {

        return BaseResult.success("");
    }

    @RequiresPermissions("sys:user:editVM")
    @RequestMapping(value = "/editVM/{id}")
    public BaseResult editVM(@PathVariable("id") Long id, HttpServletRequest request, HttpServletResponse response){

        return BaseResult.success("editVM");
    }

    @RequiresPermissions("sys:user:edit")
    @RequestMapping(value = "/edit")
    public BaseResult edit(UpmsUser upmsUser, HttpServletRequest request, HttpServletResponse response){

        return BaseResult.success("");
    }

    @RequestMapping(value = "/remove")
    public BaseResult remove(HttpServletRequest request, HttpServletResponse response){

        return BaseResult.success("");
    }

    @RequiresPermissions("sys:user:changePassword")
    @RequestMapping(value = "/changePassword")
    public BaseResult changePassword(HttpServletRequest request, HttpServletResponse response){

        return BaseResult.success("");
    }

}
