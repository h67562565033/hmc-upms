package com.huimei.hmc.upms.sys.web.controller;

import com.huimei.hmc.common.base.BaseController;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.crypto.MacProvider;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.util.Date;

/**
 * Created by Huangyuheng on 2017/7/6.
 */
@Controller
@RequestMapping("")
public class IndexController extends BaseController {

    @RequestMapping(value = "/")
    @ResponseBody
    public String index(HttpServletRequest request, HttpServletResponse response){
        logger.info("index");

        return "index";
    }

}
