package com.huimei.hmc.upms.sys.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.huimei.hmc.upms.sys.api.IUpmsResourceService;
import com.huimei.hmc.upms.sys.dao.mapper.UpmsResourceMapper;
import com.huimei.hmc.upms.sys.pojo.model.UpmsResource;
import com.huimei.hmc.upms.sys.pojo.model.UpmsUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by huangyuheng on 2017/7/18.
 */
@Service
public class UpmsResourceServiceImpl implements IUpmsResourceService {

    @Autowired
    private UpmsResourceMapper upmsResourceMapper;

    @Override
    public int saveUpmsResource(UpmsResource upmsResource) {
        return upmsResourceMapper.insertSelective(upmsResource);
    }

    @Override
    public PageInfo listUpmsResource(String username, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        PageInfo<UpmsUser> pageInfo = new PageInfo(upmsResourceMapper.selectAll());
        return pageInfo;
    }

    @Override
    public int updateUpmsResource(UpmsResource upmsResource) {
        return upmsResourceMapper.updateByPrimaryKeySelective(upmsResource);
    }

    @Override
    public UpmsResource getUpmsResourceById(Long id) {
        return upmsResourceMapper.selectByPrimaryKey(id);
    }

    @Override
    public int removeUpmsResourceById(Long id) {
        return upmsResourceMapper.deleteByPrimaryKey(id);
    }


}
