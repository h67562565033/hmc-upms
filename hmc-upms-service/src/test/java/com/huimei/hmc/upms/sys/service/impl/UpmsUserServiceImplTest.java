package com.huimei.hmc.upms.sys.service.impl;

import com.huimei.hmc.upms.sys.api.IUpmsUserService;
import com.huimei.hmc.upms.sys.pojo.model.UpmsUser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

/**
 * @author huangyuheng 2018/5/11
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({
        "classpath:spring/applicationContext-core.xml",
        "classpath:spring/applicationContext-mybatis.xml",
})
public class UpmsUserServiceImplTest {

    @Autowired
    private IUpmsUserService upmsUserService;

    @Test
    public void saveUpmsUser() throws Exception {

        UpmsUser upmsUser = new UpmsUser();
        upmsUser.setUsername("test1");
        upmsUser.setPassword("123456");

        upmsUserService.saveUpmsUser(upmsUser);
    }

}