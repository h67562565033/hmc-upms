package com.huimei.hmc.upms;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by huangyuheng on 2017/11/8.
 */
public class LogTest {

    private static final Logger logger = LoggerFactory.getLogger(LogTest.class);

    public static void main(String[] args) {

        logger.trace("trace");
        logger.debug("debug");
        logger.info("info");
        logger.warn("warn");
        logger.error("error");


    }

}
