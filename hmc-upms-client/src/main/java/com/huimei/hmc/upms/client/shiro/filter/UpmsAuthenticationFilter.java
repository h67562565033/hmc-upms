package com.huimei.hmc.upms.client.shiro.filter;

import com.alibaba.fastjson.JSONObject;
import com.huimei.hmc.common.base.BaseResult;
import com.huimei.hmc.upms.client.common.constant.UpmsClientConstant;
import com.huimei.hmc.common.util.PropertiesFileUtil;
import com.huimei.hmc.common.util.RedisUtil;
import com.huimei.hmc.upms.client.util.RequestParameterUtil;
import com.huimei.hmc.upms.sys.api.IUpmsUserService;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.AuthenticationFilter;
import org.apache.shiro.web.util.WebUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Huangyuheng on 2017/7/7.
 */
public class UpmsAuthenticationFilter extends AuthenticationFilter {

    private static final Logger logger = LoggerFactory.getLogger(UpmsAuthenticationFilter.class.getName());

    /**
     * 局部会话key
     */
    private final static String HMC_UPMS_CLIENT_SESSION_ID = "hmc-upms-client-session-id";

    /**
     * 单点同一个code所有局部会话key
     */
    private final static String HMC_UPMS_CLIENT_SESSION_IDS = "hmc-upms-client-session-ids";

    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {

        Subject subject = getSubject(request, response);
        Session session = subject.getSession();
        // 判断请求类型
        String upmsType = PropertiesFileUtil.getInstance(UpmsClientConstant.HMC_UPMS_CLIENT).get(UpmsClientConstant.UPMS_TYPE);
        session.setAttribute(UpmsClientConstant.UPMS_TYPE, upmsType);

        if (UpmsClientConstant.UPMS_TYPE_CLIENT.equals(upmsType)) {
            return validateClient(request, response);
        } else if(UpmsClientConstant.UPMS_TYPE_SERVER.equals(upmsType)) {
            return subject.isAuthenticated();
        } else {
            return false;
        }
    }

    @Override
    protected boolean onAccessDenied(ServletRequest servletRequest, ServletResponse servletResponse) throws Exception {
        StringBuffer ssoServerUrl = new StringBuffer(PropertiesFileUtil.getInstance("hmc-upms-client").get("hmc.upms.sso.server.url"));

        // server需要登录
        String upmsType = PropertiesFileUtil.getInstance(UpmsClientConstant.HMC_UPMS_CLIENT).get(UpmsClientConstant.UPMS_TYPE);

        if (UpmsClientConstant.UPMS_TYPE_SERVER.equals(upmsType)) {
            WebUtils.toHttp(servletResponse).sendRedirect(ssoServerUrl.append("/sso/login").toString());
            return false;
        }

        ssoServerUrl.append("/sso/index").append("?").append("appId").append("=").append(PropertiesFileUtil.getInstance(UpmsClientConstant.HMC_UPMS_CLIENT).get(UpmsClientConstant.UPMS_APP_ID));
        // 回跳地址
        HttpServletRequest httpServletRequest = WebUtils.toHttp(servletRequest);
        StringBuffer backurl = httpServletRequest.getRequestURL();
        String queryString = httpServletRequest.getQueryString();
        if (StringUtils.isNotBlank(queryString)) {
            backurl.append("?").append(queryString);
        }
        ssoServerUrl.append("&").append("backurl").append("=").append(URLEncoder.encode(backurl.toString(), "utf-8"));
        WebUtils.toHttp(servletResponse).sendRedirect(ssoServerUrl.toString());
        return false;
    }

    private boolean validateClient(ServletRequest request, ServletResponse response){

        Subject subject = getSubject(request, response);
        Session session = subject.getSession();
        String sessionId = session.getId().toString();
        int timeOut = (int) session.getTimeout() / 1000;

        // 判断局部会话是否登录
        String cacheClientSession = RedisUtil.get(HMC_UPMS_CLIENT_SESSION_ID + "_" + session.getId());
        if (StringUtils.isNotBlank(cacheClientSession)) {
            // 更新code有效期
            RedisUtil.set(HMC_UPMS_CLIENT_SESSION_ID + "_" + sessionId, cacheClientSession, timeOut);
            Jedis jedis = RedisUtil.getJedis();
            jedis.expire(HMC_UPMS_CLIENT_SESSION_IDS + "_" + cacheClientSession, timeOut);
            jedis.close();
            // 移除url中的code参数
            if (null != request.getParameter("code")) {
                String backUrl = RequestParameterUtil.getParameterWithOutCode(WebUtils.toHttp(request));
                HttpServletResponse httpServletResponse = WebUtils.toHttp(response);
                try {
                    httpServletResponse.sendRedirect(backUrl.toString());
                } catch (IOException e) {
                    logger.error("局部会话已登录，移除code参数跳转出错：", e);
                }
            } else {
                return true;
            }
        }
        // 判断是否有认证中心code
        String code = request.getParameter("upmsCode");
        // 已拿到code
        if (StringUtils.isNotBlank(code)) {
            // HttpPost去校验code
            try {
                StringBuffer sso_server_url = new StringBuffer(PropertiesFileUtil.getInstance(UpmsClientConstant.HMC_UPMS_CLIENT).get(UpmsClientConstant.UPMS_SSO_SERVER_URL));
                CloseableHttpClient httpclient = HttpClients.createDefault();
                HttpPost httpPost = new HttpPost(sso_server_url.toString() + "/sso/code");

                List<NameValuePair> nameValuePairs = new ArrayList<>();
                nameValuePairs.add(new BasicNameValuePair("code", code));
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                CloseableHttpResponse httpResponse = httpclient.execute(httpPost);
                if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                    HttpEntity httpEntity = httpResponse.getEntity();
                    JSONObject result = JSONObject.parseObject(EntityUtils.toString(httpEntity));
                    if (BaseResult.SUCCESS == result.getIntValue("code") && result.getString("data").equals(code)) {

                        // code校验正确，创建局部会话
                        RedisUtil.set(HMC_UPMS_CLIENT_SESSION_ID + "_" + sessionId, code, timeOut);

                        // 保存code对应的局部会话sessionId，方便退出操作
                        RedisUtil.sadd(HMC_UPMS_CLIENT_SESSION_IDS + "_" + code, sessionId, timeOut);

                        logger.debug("当前code={}，对应的注册系统个数：{}个", code, RedisUtil.getJedis().scard(HMC_UPMS_CLIENT_SESSION_IDS + "_" + code));

                        // 移除url中的token参数
                        String backUrl = RequestParameterUtil.getParameterWithOutCode(WebUtils.toHttp(request));
                        // 返回请求资源
                        try {
                            // client无密认证
                            String username = request.getParameter("upmsUsername");
                            subject.login(new UsernamePasswordToken(username, ""));
                            HttpServletResponse httpServletResponse = WebUtils.toHttp(response);
                            httpServletResponse.sendRedirect(backUrl.toString());
                            return true;
                        } catch (IOException e) {
                            logger.error("已拿到code，移除code参数跳转出错：", e);
                        }
                    } else {
                        logger.warn(result.getString("data"));
                    }
                }
            } catch (IOException e) {
                logger.error("验证token失败：", e);
            }
        }

        return false;
    }

}
