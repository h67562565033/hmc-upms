package com.huimei.hmc.upms.client.common.constant;

/**
 * Created by huangyuheng on 2017/7/12.
 */
public class UpmsClientConstant {

    /**
     * 配置文件名
     */
    public static final String HMC_UPMS_CLIENT = "hmc-upms-client";

    /**
     * 类型
     */
    public static final String UPMS_TYPE = "hmc.upms.type";

    /**
     * 类型 -- 客户端
     */
    public static final String UPMS_TYPE_CLIENT = "client";

    /**
     * 类型 -- 服务端
     */
    public static final String UPMS_TYPE_SERVER = "server";

    /**
     * appId
     */
    public static final String UPMS_APP_ID = "hmc.upms.appId";

    /**
     * sso服务器地址
     */
    public static final String UPMS_SSO_SERVER_URL = "hmc.upms.sso.server.url";

}
