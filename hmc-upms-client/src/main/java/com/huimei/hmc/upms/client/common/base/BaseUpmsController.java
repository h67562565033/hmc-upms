package com.huimei.hmc.upms.client.common.base;

import com.huimei.hmc.common.base.BaseController;
import com.huimei.hmc.common.base.BaseResult;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by huangyuheng on 2017/11/14.
 */
public class BaseUpmsController extends BaseController {

    /**
     * 统一无权限异常处理
     *
     * @param request
     * @param response
     * @param exception
     */
    @ExceptionHandler({UnauthorizedException.class, AuthorizationException.class})
    @ResponseBody
    public BaseResult unauthorizedException(HttpServletRequest request, HttpServletResponse response, Exception exception) {

        logger.error("无权限：", exception);

        return BaseResult.fail("你没有操作该功能的权限");
    }

}
