package com.huimei.hmc.upms.client.shiro.session;

import org.apache.shiro.session.mgt.SimpleSession;

/**
 * Created by Huangyuheng on 2017/7/7.
 */
public class UpmsSession extends SimpleSession {

    public enum OnlineStatus {
        ONLINE("在线"),
        OFFLINE("离线"),
        FORCE_LOGOUT("强制退出");

        private final String info;

        OnlineStatus(String info) {
            this.info = info;
        }

        public String getInfo() {
            return info;
        }
    }

    /**
     * 用户浏览器类型
     */
    private String userAgent;

    /**
     * 在线状态
     */
    private OnlineStatus status = OnlineStatus.OFFLINE;

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public OnlineStatus getStatus() {
        return status;
    }

    public void setStatus(OnlineStatus status) {
        this.status = status;
    }

}
